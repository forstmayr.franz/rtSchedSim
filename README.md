# rtSchedSim

## Usage

Show help

    python3 examples/schedTest.py -h

    usage: Test several realtime scheduling methods [-h] [-p] [-t TRIES]
                                                [--min MINCPU] [--max MAXCPU]
                                                [-c CORES] [-r RTTICKS]
                                                [-a ATUPT] [-l LANGUAGE]
                                                [-j THREADS] [-n NTASKS]

    optional arguments:
    -h, --help    show this help message and exit
    -p            Create plots (default: False)
    -t TRIES      Number of tests for each cpu load setting (default: 100)
    --min MINCPU  Minimum realtime workload (default: 1)
    --max MAXCPU  Maximum realtime workload (default: 100)
    -c CORES      Number of CPU Cores (default: 4)
    -r RTTICKS    Realtime Ticks (default: 4)
    -a ATUPT      Atomic time units within one tick (default: 8)
    -l LANGUAGE   Language of plots [de, en] (default: de)
    -j THREADS    Number of threads to use for simulation (default: 4)
    -n NTASKS     Number of tasks to simulate (default: 0)

Without any further arguments the default setting is used. It's recommended to modify the parameters as needed and add the '-p' switch if plots should be created.
The default setting can take up to a few hours time. 