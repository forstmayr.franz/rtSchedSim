import sys
import os
import numpy as np
import pandas as pd
import argparse
import datetime
import concurrent.futures as conF
from pebble import ProcessPool

sys.path.insert(0, os.path.abspath('.'))

from rtSchedSim.tasks import TaskFactory
from rtSchedSim.cpu import CPUFactory
from rtSchedSim.plotResults import plotResults
from rtSchedSim.schedulerBase import Scheduler, SchedulerFactory, SchedulerTester
import rtSchedSim.schedulers as schedulers
    
def runTest(rt, args, schedFac):
    taskFactory = TaskFactory(rt / 100, args.cores, args.atupt, args.rtTicks, np.random.uniform, args.nTasks)
    schedTest = SchedulerTester(schedFac, args.tries, taskFactory)
    schedTest.run()
    return schedTest.result

def schedTest(args : argparse.Namespace):

    if args.nTasks == 0:
        rtTaskUsage = np.arange(args.mincpu, args.maxcpu + 1)
    else:
        mincpu = int(np.ceil(args.nTasks / (args.atupt * args.rtTicks * args.cores) * 100))
        rtTaskUsage = np.arange(np.max([args.mincpu, mincpu]), args.maxcpu + 1)

    cpuFac = CPUFactory(args.cores, timeslots=args.atupt * args.rtTicks, cycles = args.rtTicks)

    schedulerFactories = [
            SchedulerFactory(schedulers.EarliestDeadlineScheduler,      cpuFactory=cpuFac, slots = args.atupt, cycles = args.rtTicks),
            SchedulerFactory(schedulers.LazyLeastLaxityScheduler,       cpuFactory=cpuFac, slots = args.atupt, cycles = args.rtTicks),
            SchedulerFactory(schedulers.LeastLaxityScheduler,           cpuFactory=cpuFac, slots = args.atupt, cycles = args.rtTicks),
            SchedulerFactory(schedulers.ModifiedLeastLaxityScheduler,   cpuFactory=cpuFac, slots = args.atupt, cycles = args.rtTicks)
            ]
    schedulerNames = [schedFac.schedulerType.abbrev() for schedFac in schedulerFactories]

    results = pd.DataFrame({'scheduler': schedulerNames * len(rtTaskUsage),
                            'rt_task_usage': np.repeat(rtTaskUsage, len(schedulerNames)), 
                            'finished': np.nan,
                            'tasks' : np.nan,
                            'ctx_switch_ratio': np.nan})
    
    results.set_index(['scheduler', 'rt_task_usage'], inplace=True)
    
    for schedFac in schedulerFactories:
        print(schedFac.schedulerType.abbrev())
        with ProcessPool(args.threads) as executor:
            try:
                promises = {executor.schedule(runTest, (rt, args, schedFac)): rt for rt in rtTaskUsage}
                for promise in conF.as_completed(promises):
                    rt = promises[promise]
                    res = promise.result()
                    results.loc[(schedFac.schedulerType.abbrev(), rt), :] = res
            except KeyboardInterrupt:
                [p.cancel() for p in promises]
                break



    results.finished = results.finished / args.tries

    filename = str(datetime.datetime.now()).replace(':', '-')

    store = pd.HDFStore(filename + '.h5')
    store.put('results', results, format='table')
    store.get_storer('results').attrs.metadata = args
    store.close()

    if args.plot:
        plotResults(results, filename, args.language)




if __name__ == "__main__":
    parser =argparse.ArgumentParser('Test several realtime scheduling methods', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-p',       dest='plot',    action='store_true',        help='Create plots')

    parser.add_argument('-t',       dest='tries',   type=int,   default=100,    help="Number of tests for each cpu load setting")
    parser.add_argument('--min',    dest='mincpu',  type=int,   default=1,      help='Minimum realtime workload')
    parser.add_argument('--max',    dest='maxcpu',  type=int,   default=100,    help='Maximum realtime workload')
    parser.add_argument('-c',       dest='cores',   type=int,   default=4,      help='Number of CPU Cores')
    parser.add_argument('-r',       dest='rtTicks', type=int,   default=4,      help='Realtime Ticks')
    parser.add_argument('-a',       dest='atupt',   type=int,   default=8,      help='Atomic time units within one tick')
    parser.add_argument('-l',       dest='language',type=str,   default='de',   help='Language of plots [de, en]')
    parser.add_argument('-j',       dest='threads', type=int,   default=4,      help='Number of threads to use for simulation')
    parser.add_argument('-n',       dest='nTasks',  type=int,   default=0,      help='Number of tasks to simulate')

    args = parser.parse_args()

    schedTest(args)

        