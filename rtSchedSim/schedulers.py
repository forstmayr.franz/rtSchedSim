"""
.. currentmodule:: rtSchedSim.schedulers

schedulers (:mod:`rtSchedSim.schedulers`)
=============================================
Provides several scheduler classes.


Classes
---------------------

.. autosummary::
    :toctree: generated/

    LeastLaxityScheduler
    LazyLeastLaxityScheduler
    EarliestDeadlineScheduler
    ModifiedLeastLaxityScheduler
"""

import numpy as np
import pandas as pd

from .cpu import CPUFactory
from .schedulerBase import Scheduler


class LeastLaxityScheduler(Scheduler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def abbrev():
        return 'LL'

    def schedule(self):
        self.tasks['running'] = False

        total_slots = self.slots * self.cycles
        for t in range(total_slots):
            self.tasks['scheduled'] = False
            self.tasks['rem'] = self.tasks.wcet - self.tasks.used
            self.tasks['laxity'] = self.tasks.deadline - self.tasks.rem - t
            for cpu in self.cpus:
                self.select_ll_slot(cpu, t)

        for c in range(len(self.cpus)):
            self.cpuslots[c] = self.cpus[c].slots

        ctx_switches = sum([cpu.ctx_switches for cpu in self.cpus])
        self.tasks['finished'] = self.tasks.wcet == self.tasks.used

        return self.tasks.finished.all(), len(self.tasks), ctx_switches

    def select_ll_task(self, cpu, dl, t):
        task = self.tasks.loc[
            (self.tasks.scheduled == False) & 
            (self.tasks.finished == False) & 
            (self.tasks.rem <= self.tasks.deadline - t)].sort_values('laxity')
        if task.empty:
            return
        task = task.iloc[0]
        if cpu.is_free(t):
            stop = min(t + task.rem, dl)
            cpu.slots[t:stop] = task.name
            self.tasks.loc[task.name, 'used'] += stop - t
            self.tasks.loc[task.name, 'scheduled'] = True
            return

    def select_ll_slot(self, cpu, time):
        active_task = None
        if time == 0:
            this_task_laxity = cpu.timeslots + 1
        else:
            if cpu.is_free(time - 1):
                return
            active_task = self.tasks.iloc[int(cpu.get_task(time - 1))]
            this_task_laxity = active_task.laxity
            if active_task.rem == 0:
                this_task_laxity = cpu.timeslots + 1

        df_dl = self.tasks.loc[ 
            (self.tasks.scheduled == False) & 
            (self.tasks.running == False) & +
            (self.tasks.rem <= self.tasks.deadline - time) & 
            (self.tasks.rem > 0) & 
            (self.tasks.laxity < this_task_laxity )].sort_values('laxity')

        if df_dl.empty:
            if active_task is None:
                return
            if active_task.rem == 0:
                return
            cpu.slots[time] = active_task.name
            self.tasks.loc[active_task.name, 'used'] += 1
            self.tasks.loc[active_task.name, 'scheduled'] = True
            return

        task = df_dl.iloc[0]
        self.tasks.loc[task.name, 'scheduled'] = True
        self.tasks.loc[task.name, 'running'] = True
        if time > 0:
            self.tasks.loc[active_task.name, 'running'] = False
        cpu.slots[time] = task.name
        self.tasks.loc[task.name, 'used'] += 1


class LazyLeastLaxityScheduler(LeastLaxityScheduler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.preferred_core = {cpu:np.nan for cpu in self.cpus}
        self.lll_tasks = {cpu: [] for cpu in self.cpus}

    def abbrev():
        return 'LLL'

    def assign_tasks(self, cpu, tasks, deadline):
        start = deadline - cpu.timeslots // cpu.cycles
        for t in tasks:
            stop = min(start + self.tasks.rem.iloc[t], deadline)
            cpu.slots[start:stop] = t
            self.tasks.loc[t, 'used'] += stop - start
            self.tasks.loc[t, 'scheduled'] = True
            start = stop

    def schedule(self):
        self.tasks['cpu'] = np.nan
        self.tasks['rem'] = self.tasks.wcet
        
        for dl in np.arange(1,self.cycles+1) * self.slots :
            self.tasks['rem'] = self.tasks.wcet - self.tasks.used
            self.tasks['laxity'] = self.tasks.deadline - self.tasks.rem - dl + self.slots
            self.tasks['scheduled'] = False
            self.tasks['lazy_assigned'] = False

            # Find tasks which needs to be finished the next deadline and fill cpu capacity
            for cpu in self.cpus:
                self.select_lll(cpu, dl)
            
            # If one of the tasks is already running on a cpu, keep it running - Basically the "lazy" in LLL
            for cpu in self.cpus:
                if not np.isnan(self.preferred_core[cpu]):
                    self.assign_tasks(self.cpus[int(self.preferred_core[cpu])], self.remove_lll_tasks(cpu), dl)

            # Assign the remaining tasks to the remaining cpu
            for cpu in self.cpus:
                if cpu.is_free(dl - self.slots):
                    for v_cpu in self.cpus:
                        if self.lll_tasks[v_cpu] != []:
                            self.assign_tasks(cpu, self.remove_lll_tasks(cpu), dl)
                            break
            
            # If a CPU finishes all the tasks required for the next deadline, pick the one with least laxity.
            for t in range(dl - self.slots, dl):
                for cpu in self.cpus:
                    if cpu.is_free(t):
                        self.select_ll_task(cpu, dl, t)

            self.tasks.cpu = np.nan
            for i, cpu in enumerate(self.cpus):
                try:
                    self.tasks.loc[int(cpu.get_task(dl - 1)), 'cpu'] = i
                except ValueError:
                    pass
                self.cpuslots[i] = cpu.slots

            self.tasks['finished'] = (self.tasks.used == self.tasks.wcet)

        ctx_switches = sum([cpu.ctx_switches for cpu in self.cpus])
        return self.tasks.finished.all(), len(self.tasks), ctx_switches

    def remove_lll_tasks(self, cpu):
        t = self.lll_tasks[cpu]
        self.lll_tasks[cpu] = []
        self.preferred_core[cpu] = np.nan
        return t

    def select_lll(self, cpu, deadline):
        time_alloc = 0
        sticky_task = np.nan
        next_deadline = deadline
        slots = cpu.timeslots / cpu.cycles
        df_dl = self.tasks.loc[
            (self.tasks.finished == False) & 
            (self.tasks.deadline == deadline) & 
            (self.tasks.lazy_assigned == False) & 
            (self.tasks.rem <= slots - time_alloc)].sort_values('laxity')

        if df_dl.empty:
            next_deadline = deadline + slots
        cpu_id = np.min(self.tasks.cpu.iloc[self.lll_tasks[cpu]])
        while True:
            if next_deadline == deadline:
                df_dl = self.tasks.loc[
                    (self.tasks.finished == False) & 
                    (self.tasks.deadline == deadline) & 
                    (self.tasks.lazy_assigned == False) & 
                    (self.tasks.rem <= slots - time_alloc)].sort_values('laxity')
            else:
                df_dl = self.tasks.loc[
                    (self.tasks.finished == False) & 
                    (self.tasks.deadline == next_deadline) & 
                    (self.tasks.lazy_assigned == False)].sort_values('laxity')
            if df_dl.empty:
                return
            # Decision between brand new and already started tasks. If you already have an task with preferred cpu in stack, take a brand new one, so the started one 
            # gets a chance to be run on an own core and avoids a context switch
            if not np.isnan(self.preferred_core[cpu]):
                df_dl = df_dl.loc[df_dl.used == df_dl.used.iloc[0]]
                task = df_dl.loc[np.isnan(df_dl.cpu)]
                if task.empty:
                    task = df_dl.iloc[0]
                else:
                    task = task.iloc[0]

                self.lll_tasks[cpu].append(task.name)
                self.tasks.loc[task.name, 'lazy_assigned'] = True
                time_alloc = np.sum(self.tasks.wcet[self.lll_tasks[cpu]])

                if time_alloc == deadline:
                    return
                
                continue

            task = df_dl.iloc[0]
            if time_alloc + task.rem <= deadline:
                if np.isnan(task.cpu):
                    self.lll_tasks[cpu].append(task.name)
                else:
                    self.lll_tasks[cpu].insert(0, task.name)
                    self.preferred_core[cpu] = task.cpu
                self.tasks.loc[task.name, 'lazy_assigned'] = True
                time_alloc = np.sum(self.tasks.wcet[self.lll_tasks[cpu]])

                if time_alloc == deadline:
                    return

            if deadline != next_deadline:
                return
            
            pass


class EarliestDeadlineScheduler(Scheduler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def abbrev():
        return 'EDF'

    def schedule(self):
        self.tasks['scheduled'] = False
        self.tasks['running'] = False

        total_slots = self.slots * self.cycles
        for t in range(total_slots):
            self.tasks.rem = self.tasks.wcet - self.tasks.used
            for cpu in self.cpus:
                if cpu.is_free(t):
                    self.select_edf_task(cpu, t)

        for c in range(len(self.cpus)):
            self.cpuslots[c] = self.cpus[c].slots
        
        ctx_switches = sum([cpu.ctx_switches for cpu in self.cpus])
        self.tasks['finished'] = self.tasks.wcet == self.tasks.used
        return self.tasks.finished.all(), len(self.tasks), ctx_switches

    def select_edf_task(self, cpu, time):
        df_dl = self.tasks.loc[
            (self.tasks.scheduled == False) & 
            (self.tasks.running == False) &
            (self.tasks.rem > 0) & 
            (self.tasks.rem <= self.tasks.deadline - time)].sort_values('deadline')
        if df_dl.empty:
            return
        task = df_dl.iloc[0]
        self.tasks.loc[task.name, 'scheduled'] = True
        self.tasks.loc[task.name, 'used'] += task.rem
        cpu.slots[time:time+task.rem] = task.name
        return


class ModifiedLeastLaxityScheduler(EarliestDeadlineScheduler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def abbrev():
        return 'MLL'

    def schedule(self):
        self.tasks['running'] = False
        self.tasks['laxity'] = self.tasks.deadline - self.tasks.wcet

        total_slots = self.slots * self.cycles

        for t in range(total_slots):
            self.tasks['scheduled'] = False
            self.tasks['rem'] = self.tasks.wcet - self.tasks.used
            self.tasks['laxity'] = self.tasks.deadline - self.tasks.rem - t
            for cpu in self.cpus:
                if cpu.is_free(t):
                    self.select_mll_task(cpu, t)


        for c in range(len(self.cpus)):
            self.cpuslots[c] = self.cpus[c].slots

        ctx_switches = sum([cpu.ctx_switches for cpu in self.cpus])
        self.tasks['finished'] = self.tasks.wcet == self.tasks.used

        return self.tasks.finished.all(), len(self.tasks), ctx_switches

    def select_mll_task(self, cpu, time):
        active_task = None
        if time == 0:
            this_task_laxity = cpu.timeslots + 1
        else:
            if cpu.is_free(time - 1):
                return
            active_task = self.tasks.iloc[int(cpu.get_task(time - 1))]
            this_task_laxity = active_task.laxity
            if active_task.rem == 0:
                this_task_laxity = cpu.timeslots + 1

        df_dl = self.tasks.loc[
            (self.tasks.scheduled == False) & 
            (self.tasks.running == False) & 
            (self.tasks.rem <= self.tasks.deadline - time) & 
            (self.tasks.rem > 0) & 
            (self.tasks.laxity < this_task_laxity )].sort_values('laxity')
        if df_dl.empty:
            if active_task is None:
                return
            if active_task.rem == 0:
                return
            cpu.slots[time] = active_task.name
            self.tasks.loc[active_task.name, 'used'] += 1
            self.tasks.loc[active_task.name, 'scheduled'] = True
            return

        df_dl = df_dl.loc[df_dl.laxity == df_dl.laxity.iloc[0]]
        if len(df_dl) > 1:
            self.select_edf_task(cpu, time)
            self.tasks.loc[int(cpu.get_task(time)), 'running'] = True
            if time > 0:
                self.tasks.loc[active_task.name, 'running'] = False
            return

        task = df_dl.iloc[0]
        self.tasks.loc[task.name, 'scheduled'] = True
        self.tasks.loc[task.name, 'running'] = True
        if time > 0:
            self.tasks.loc[active_task.name, 'running'] = False
        cpu.slots[time] = task.name
        self.tasks.loc[task.name, 'used'] += 1