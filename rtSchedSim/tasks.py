"""
.. currentmodule:: rtSchedSim.tasks

tasks (:mod:`rtSchedSim.tasks`)
=============================================
Provides task generation algorithm.


Classes
---------------------

.. autosummary::
    :toctree: generated/

    TaskFactory
"""

import numpy as np
import numpy.random as npr
import pandas as pd

from functools import lru_cache

class TaskFactory():
    def __init__(self, rtTaskUsage : float, nCores : int, atomicTimeUnitsPerTick : int, rtTicks : int, lenDistribution, nTasks=0):
        self.rtTaskUsage = rtTaskUsage
        self.nCores = nCores
        self.rtTicks = rtTicks
        self.atomicTimeUnitsPerTick = atomicTimeUnitsPerTick
        self.lenDistribution = lenDistribution
        if nTasks == 0:
            self.getTasks = self._getRandomNumberTasks
        else:
            self.nTasks = nTasks
            self.getTasks = self._getFixedNumberTasks

    @property
    def taskLen(self):
        task_len = np.abs(self.lenDistribution(size=(self.nTasks))) 
        task_len = task_len / np.sum(task_len)
        return task_len

    @lru_cache(None)
    def _getRandomNumberTasks(self, nTry: int):
        rs = npr.RandomState(nTry)
        totalTimeUnits = self.nCores * self.atomicTimeUnitsPerTick * self.rtTicks
        tasks_deadline = rs.multinomial(int(totalTimeUnits * self.rtTaskUsage) , rs.dirichlet([1] * self.rtTicks))
        cumsum_available = np.arange(self.rtTicks+1)[1:] * self.nCores * self.atomicTimeUnitsPerTick

        while True:
            if ((np.cumsum(tasks_deadline) <= cumsum_available).all()):
                break
            else:
                rs.shuffle(tasks_deadline)
        
        tasks = []
        for c in range(self.rtTicks):
            tasks_cycle = []
            tsum = tasks_deadline[c]
            while(tsum > 0):
                x = rs.randint(1, min((c+1) * self.atomicTimeUnitsPerTick, tsum) + 1)
                tsum -= x
                tasks.append(((c+1) * self.atomicTimeUnitsPerTick, x))

        return pd.DataFrame(tasks, columns=['deadline', 'wcet'])
    
    @lru_cache(None)
    def _getFixedNumberTasks(self, nTry: int):
        rs = npr.RandomState(nTry)
        totalTimeUnits = self.nCores * self.atomicTimeUnitsPerTick * self.rtTicks
        loopcnt = 0
        while True:
            loopcnt += 1
            
            tasks = np.ones(self.nTasks, dtype=int)
            tasks += rs.multinomial(int(totalTimeUnits * self.rtTaskUsage - self.nTasks), self.taskLen)

            deadline_min = np.ceil(tasks / self.atomicTimeUnitsPerTick)
            deadline = np.zeros(deadline_min.shape, dtype=np.int)
            try:
                for i,dlm in enumerate(deadline_min):
                    deadline[i] = rs.randint(low=dlm, high=self.rtTicks + 1)

                #deadline = npr.randint(0, self.rtTicks + 1, size=deadline_min.shape)
                #deadline = np.maximum(deadline, deadline_min)
            except ValueError:
                continue

            deadline = deadline * self.atomicTimeUnitsPerTick
            tasks = pd.DataFrame({'wcet': tasks, 'deadline': deadline})
            
            tasks_grouped = tasks.groupby(by='deadline').wcet.sum()

            if (tasks_grouped.index * self.nCores >= tasks_grouped.cumsum()).all():
                break

        return tasks
