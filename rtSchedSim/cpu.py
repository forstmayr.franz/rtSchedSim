"""
.. currentmodule:: rtSchedSim.cpu

cpu (:mod:`rtSchedSim.cpu`)
=============================================
Provides CPU Class.


Classes
---------------------

.. autosummary::
    :toctree: generated/

    CPUFactory
    CPU
"""

import numpy as np
import pandas as pd

class CPU:
    def __init__(self, timeslots : int, cycles : int):
        """CPU class

        Parameters
        ----------
        timeslots : int
            Total number of atomic time units used in simulation
        cycles : int
            Number of realtime cycles
        """
        self.timeslots = timeslots
        self.cycles = cycles
        self.slots = np.zeros(timeslots)
        self.slots.fill(np.nan)
       

    def is_free(self, time):
        """Check if the CPU is free at a certain time

        Parameters
        ----------
        time : int
            timeslot

        Returns
        -------
        free : bool
            returns True if CPU is free
        """
        return np.isnan(self.slots[time])

    def get_task(self, time):
        """Get task running at a certain time

        Parameters
        ----------
        time : int
            timeslot

        Returns
        -------
        task_id : int
            the ID of the running task
        """
        return self.slots[time]

    @property
    def ctx_switches(self):
        """
        Number of context switches

        Returns
        -------
        ctx_switches : int
            the number of context switches
        """
        if np.isnan(self.slots).all():
            return 0
        ret = np.clip(np.abs(np.diff(self.slots)), 0, 1)
        ret = int(np.nansum(ret)) + 1
        return ret


class CPUFactory():
    def __init__(self, nCores : int, **cpuArgs : dict):
        """Initializer to create CPU instances

        Parameters
        ----------
        nCores : int
            Number of cores
        """
        self.nCores = nCores
        self.cpuArgs = cpuArgs
        
    def getCPUs(self) -> [CPU]:
        """Returns CPU instances

        Returns
        -------
        [CPU]
            CPU class
        """
        return [CPU(**self.cpuArgs) for i in range(self.nCores)]
