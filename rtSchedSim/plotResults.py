"""
.. currentmodule:: rtSchedSim.plotResults

plotResults (:mod:`rtSchedSim.plotResults`)
=============================================
Provides plot function.


Functions
---------------------

.. autosummary::
    :toctree: generated/

    plotResults
"""

import pandas as pd
import matplotlib.pyplot as plt 
import argparse
import tikzplotlib as tpl

def plotResults(df : pd.DataFrame, filename : str, language : str):
    """Function to create plots from previously generated results

    Parameters
    ----------
    df : pd.DataFrame
        pandas dataframe, with at least the following columns
        
        - scheduler : str (index)
        - rt_task_usage : int (index)
        - ctx_switch_ratio : float
        - finished : float
    filename : str
        Filename of the output plot
    language : str
        language of the plot description [de, en]

    Raises
    ------
    NotImplementedError
        If a unknown language is choosen
    """

    if (df.tasks[0] == df.tasks).all():
        fig, ax1 = plt.subplots()
        ax3 = plt.axis()
        xaxis = ax1
        taskplot = False
    else:
        fig, ax = plt.subplots(nrows=2, sharex=True, gridspec_kw={'height_ratios': [3,1]}, figsize=(6.4, 6.4))
        ax1 = ax[0]
        ax3 = ax[1]
        ax3.grid()
        xaxis = ax3
        taskplot = True
    
    ax2 = ax1.twinx()

    schedulers = df.index.unique(0).tolist()

    if language == 'de':
        xaxis.set_xlabel('Echtzeit Auslastung [%]')
        ax1.set_ylabel('Scheduling erfolgreich [%]')
        ax2.set_ylabel('Kontextwechsel pro Echtzeit Task')
        str_finished = 'Erfolgreich '
        str_ctx = 'Kontextwechsel '
        if taskplot:
            str_tasks = 'Tasks'
            ax3.set_ylabel('Anzahl der Tasks')
    elif language == 'en':
        xaxis.set_xlabel('Realtime task utilization [%]')
        ax1.set_ylabel('Scheduling succeded [%]')
        ax2.set_ylabel('Context switches per realtime task')
        str_finished = 'succeeded '
        str_ctx = 'CTX switches '
        if taskplot:
            str_tasks = 'tasks'
            ax3.set_ylabel('Number of tasks')
    else:
        raise NotImplementedError('Language %s not implemented', language)

    ax1.grid()

    for i,s in enumerate(schedulers):
        finished, = ax1.plot(df.loc[s].finished * 100, linestyle='solid', label=str_finished + s)
        ax2.plot(df.loc[s].ctx_switch_ratio, color=finished.get_color(), linestyle='dashed', label=str_ctx + s), 

    print(df.groupby(['rt_task_usage']).tasks.mean())
    if taskplot:
        ax3.plot(df.groupby(['rt_task_usage']).tasks.mean(), color='black', label=str_tasks)

    legend = ax1.legend(loc='upper left', shadow=True, bbox_to_anchor=(0, 0.85))
    legend = ax2.legend(loc='upper left', shadow=True, bbox_to_anchor=(0, 0.5))

    fig.savefig(filename + '.png', dpi=600)
    fig.savefig(filename + '.pgf')

    tpl.save(filename + ".tex")

    

