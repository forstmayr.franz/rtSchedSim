"""
.. currentmodule:: rtSchedSim.schedulerBase

schedulerBase (:mod:`rtSchedSim.schedulerBase`)
===============================================
Provides Scheduler Class.


Classes
----------------------

.. autosummary::
    :toctree: generated/

    Scheduler
    SchedulerFactory
    SchedulerTester
"""

import numpy as np
import pandas as pd
import time

import abc

from .cpu import CPUFactory
from .tasks import TaskFactory

class Scheduler():
    """Abstract Scheduler base class
    """
    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

        self.cpus = self.cpuFactory.getCPUs()
        self.tasks['used'] = 0
        self.tasks['finished'] = False
        self.tasks['rem'] = self.tasks.wcet
        self.cpuslots = np.zeros((len(self.cpus), self.cycles * self.slots))
    

    @abc.abstractmethod
    def schedule(self):
        """Run the main scheduling algorithm
        """
        pass

    @property
    def result(self):
        return True

    @staticmethod
    def abbrev():
        """Just the abbreviation for the plots

        Returns
        -------
        abbreviation : str
            The abbreviation to be used in plots
        """
        return 'Abstract Scheduler'



class SchedulerFactory():
    """Factory for creating Scheduler instances
    """
    def __init__(self, schedulerType : Scheduler, **schedulerArgs : dict):
        """Constructor for SchedulerFactory

        Parameters
        ----------
        schedulerType : Scheduler
            The Scheduler class to create
        schedulerArgs : dict
            SchedulerArgs are directly passed to the created instance constructor
        """
        self.schedulerType = schedulerType
        self.schedulerArgs = schedulerArgs

    def getScheduler(self, **kwargs):
        """Get the scheduler instance

        Parameters
        ----------
        kwargs : dict
            kwargs are combined with SchedulerFactory's schedulerArgs and passed to 
            the new class instance

        Returns
        -------
        scheduler : Scheduler
            The Scheduler instance
        """
        return self.schedulerType(**self.schedulerArgs, **kwargs)


class SchedulerTester():
    """Helper class to abstract testing for several Schedulers
    """
    def __init__(self, schedulerFactory : SchedulerFactory, tries : int, taskFactory: TaskFactory):
        """SchedulerTester constructor

        Parameters
        ----------
        schedulerFactory : SchedulerFactory
            The instanciated schedulerFactory instance
        tries : int
            Number of test runs
        taskFactory : TaskFactory
            The instanciated taskFactory
        """
        self.schedulerFactory = schedulerFactory
        self.tries = tries
        self.taskFactory = taskFactory
        pass

    def run(self):
        """Run the main test.

        Simulates 'tries' runs with tasks created from 'taskFactory'
        """
        results = pd.DataFrame(index=np.arange(self.tries), columns = ['finished', 'tasks', 'ctx_switches'])
        for t in range(self.tries):
            tasks = self.taskFactory.getTasks(t)
            scheduler = self.schedulerFactory.getScheduler(tasks = tasks)
            results.loc[t, :] = scheduler.schedule()

        filtered = results.loc[results.finished == True]
        self._result = (len(filtered), results.tasks.mean(), filtered.ctx_switches.mean() / filtered.tasks.mean())

    @property
    def result(self):
        """Returns the result of the simulation

        Returns
        -------
        finshed : int 
            Number of successful tries
        ctx_switches_mean : float
            Average number of context switches
        """
        return self._result
