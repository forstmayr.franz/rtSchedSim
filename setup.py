import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="rtSchedSim", # Replace with your own username
    version="0.0.1",
    author="Franz Forstmayr",
    author_email="forstmayr.franz@gmail.com",
    description="Simulator for realtime scheduling methods",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/forstmayr/linux-plc/rtSchedSim.git",
    packages=setuptools.find_packages(),
    install_requires = [
        'numpy',
        'pandas',
        'matplotlib',
        'tikzplotlib'
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
